import React from 'react';
import MainRouter from './tasks-router/MainRouter';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import {rootReducer} from './tasks-redux/reducer/index';

const store = createStore(rootReducer);

const App = () => {
  return (
    <>
      <Provider store={store}>
        <MainRouter />
      </Provider>
    </>
  );
};

export default App;
