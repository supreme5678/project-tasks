import React from 'react';
import {StyleSheet, View, Button} from 'react-native';

const SideTab = ({navigation}) => {
  return (
    <View style={styles.view}>
      <Button onPress={() => navigation.goBack()} title="Go back home" />
    </View>
  );
};

export default SideTab;

const styles = StyleSheet.create({
  view: {flex: 1, alignItems: 'center', justifyContent: 'center'},
});
