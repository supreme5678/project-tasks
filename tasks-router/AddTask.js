import React from 'react';
import {StyleSheet, Text, View, Button} from 'react-native';

const AddTask = props => {
  const {navigation} = props;
  return (
    <View style={styles.view}>
      <Button onPress={() => navigation.navigate('Home')} title="Go to home" />

      <Text>ADD AddTask</Text>
    </View>
  );
};

export default AddTask;

const styles = StyleSheet.create({
  view: {flex: 1, alignItems: 'center', justifyContent: 'center'},
});
