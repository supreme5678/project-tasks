import React from 'react';
import {StyleSheet, View, Text, StatusBar} from 'react-native';
import {Header, Left, Body, Right, Button, Icon, Title} from 'native-base';

const Home = props => {
  const {navigation} = props;
  return (
    <>
      <StatusBar
        translucent
        backgroundColor="transparent"
        barStyle="dark-content"
      />
      <Header style={{backgroundColor: 'transparent'}}>
        <Left>
          <Button
            transparent
            onPress={() => {
              navigation.openDrawer();
            }}>
            <Icon name="menu" style={{color: '#000'}} />
          </Button>
        </Left>
        <Body />
        <Right />
      </Header>
      <View style={styles.view1}>
        <Text />
        <Button onPress={() => navigation.navigate('Create')}>
          <Text>Wow</Text>
        </Button>
      </View>
      <View style={styles.view2}>
        {/* <Button onPress={() => navigation.navigate('Add')} /> */}
      </View>
      <View style={styles.view}>
        <></>
      </View>
    </>
  );
};

export default Home;

const styles = StyleSheet.create({
  view: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#554',
  },
  view1: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#777',
  },
  view2: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#999',
  },
});
