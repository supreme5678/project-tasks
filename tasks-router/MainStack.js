import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Home from './Home';
import CreateTask from '../tasks-screen/CreateTask';
import {StyleSheet} from 'react-native';

const Screen = createStackNavigator();

const MainStack = props => {
  //   const {navigatin, route} = props;
  return (
    // <SafeAreaView forceInset={{top: 'always'}} style={styles.safeArea}>
    <Screen.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Screen.Screen name="Home" component={Home} />
      <Screen.Screen name="Create" component={CreateTask} />
    </Screen.Navigator>
    // </SafeAreaView>
  );
};

export default MainStack;

const styles = StyleSheet.create({});
