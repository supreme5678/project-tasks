import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
// import {createStackNavigator} from '@react-navigation/stack';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import SafeAreaView from 'react-native-safe-area-view';

import {createDrawerNavigator} from '@react-navigation/drawer';
import SideTab from './SideTab';
import MainStack from './MainStack';
import {Text, StyleSheet, StatusBar} from 'react-native';
import DrawerHeader from './DrawerHeader';
import AllTask from '../tasks-screen/AllTask';
import Calendar from '../tasks-screen/Calendar';
import Friend from '../tasks-screen/Friend';
import Profile from '../tasks-screen/Profile';
import Setting from '../tasks-screen/Setting';

const Drawer = createDrawerNavigator();

const MainRouter = () => {
  //   const navigationOptions = {
  //     headerShown: false,
  //   };

  return (
    <SafeAreaProvider>
      <StatusBar backgroundColor="transparent" translucent />
      <SafeAreaView forceInset={{top: 'always'}} style={styles.safeArea}>
        <NavigationContainer>
          {/* <Text>App</Text> */}
          <Drawer.Navigator initialRouteName="Main">
            <Drawer.Screen name="Main" component={MainStack} />
            <Drawer.Screen
              name="AllTask"
              component={AllTask}
              options={{title: 'All tasks'}}
            />
            <Drawer.Screen
              name="Calendar"
              component={Calendar}
              options={{title: 'Calendar'}}
            />
            <Drawer.Screen
              name="Friend"
              component={Friend}
              options={{title: 'Friend'}}
            />
            <Drawer.Screen
              name="Profile"
              component={Profile}
              options={{title: 'Profile'}}
            />
            <Drawer.Screen
              name="Setting"
              component={Setting}
              options={{title: 'Setting'}}
            />
            {/* <Drawer.Screen name="Notifications" component={SideTab} /> */}
            {/* <Drawer.Screen
            name="Icon"
            component={DrawerHeader}
            options={{title: 'Task'}}
          /> */}
          </Drawer.Navigator>
        </NavigationContainer>
      </SafeAreaView>
    </SafeAreaProvider>
  );
};

export default MainRouter;

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: 'transparent',
    // paddingTop: 20,
  },
});
