import {LOGIN, LOGOUT} from '../actions/authAction';

const INITIAL_STATE = {
  isLogin: false,
  token: '',
};

export const authReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        isLogin: true,
        token: action.payload,
      };
    case LOGOUT:
      return INITIAL_STATE;
    default:
      return state;
  }
};
